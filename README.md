# Library Management System

## Project Details

**Project Name:** Library Management System  
**Description:** A web application designed to help manage books in a library efficiently.  
**Developers:** Dorji Phuntsho, Sonam Tshering, Tshering Dorji  

**Authors and Roles:**  
- Dorji Phuntsho: Developer  
- Sonam Tshering: Developer  
- Tshering Dorji: Developer  

**Dependencies:**  
- Java  
- Spring Boot  
- MySQL  

## Installation Instructions

1. Install Java and MySQL.
2. Clone the repository.
3. Run the application using the command `java -jar library-management-system.jar`.

## Usage Instructions

4. Open a web browser and navigate to `http://localhost:9090`.
5. Use the provided forms to add, update, or delete books.
6. Use the search functionality to find specific books by title or author.

## Troubleshooting Instructions

7. Check the application logs for any errors.
8. Ensure that the database connection is correct.
9. Verify that the application is running correctly.

## Overview

The Library Management System (LMS) is a software application designed to manage the operations of a library. This system utilizes two main frameworks:
1. **MySQL Connector/J** for database connectivity.
2. **Custom Framework** that includes the Strategy and Command design patterns for handling various library operations.

## Frameworks Used

### 1. MySQL Connector/J (mysql-connector-j-8.3.0.jar)

**Description:**  
MySQL Connector/J is a JDBC Type 4 driver that allows Java applications to connect to MySQL databases. This driver enables the LMS to perform database operations such as querying, updating, and managing library records.

**Usage in LMS:**  
- **Database Connection:** Establishes a connection to the MySQL database to perform CRUD (Create, Read, Update, Delete) operations on library data such as books, members, and transactions.

### 2. Custom Framework (framework.jar)

**Description:**  
The custom framework provides a structured approach to implementing the Strategy and Command design patterns. These patterns help in organizing the code and making the system extensible and maintainable.

**Components:**

- **Strategy Pattern:** Defines a family of algorithms, encapsulates each one and makes them interchangeable. The Strategy pattern is used for implementing different algorithms for operations such as searching for books or calculating fines.
- **Command Pattern:** Encapsulates a request as an object, thereby allowing parameterization of clients with queues, requests, and operations. The Command pattern is used for implementing various user commands such as borrowing a book, returning a book, and registering a new member.

**Conclusion:**
The Library Management System leverages the MySQL Connector/J for efficient database operations and a custom framework implementing the Strategy and Command design patterns to manage different library functionalities. This approach ensures the system is modular, maintainable, and extensible for future enhancements.

**Usage in LMS:**

#### Strategy Pattern

**Interface:**
```java
public interface SearchStrategy {
    List<Book> search(String query);
}

**Concrete Classes:**
package com.LibraryMs.LibraryMS.strategy;

import java.util.List;
import java.util.ArrayList;
import framework.*;

import com.LibraryMs.LibraryMS.model.Book;

public class AuthorSearchStrategy implements SearchStrategy {
    @Override
    public List<Book> search(List<Book> books, String query) {
        List<Book> result = new ArrayList<>();
        for (Book book : books) {
            if (book.getAuthor().toLowerCase().contains(query.toLowerCase())) {
                result.add(book);
            }
        }
        return result;
    }
}

package com.LibraryMs.LibraryMS.strategy;

import java.util.List;
import java.util.ArrayList;
import framework.*;

import com.LibraryMs.LibraryMS.model.Book;

public class TitleSearchStrategy implements SearchStrategy {
    @Override
    public List<Book> search(List<Book> books, String query) {
        List<Book> result = new ArrayList<>();
        for (Book book : books) {
            if (book.getTitle().toLowerCase().contains(query.toLowerCase())) {
                result.add(book);
            }
        }
        return result;
    }
}


**Interface:**
public interface Command {
    void execute();
}

**Concrete Classes:**
package com.LibraryMs.LibraryMS.command;
import framework.*;
import com.LibraryMs.LibraryMS.model.Book;
import com.LibraryMs.LibraryMS.service.BookService;

public class AddBookCommand implements Command {
    private BookService bookService;
    private Book book;

    public AddBookCommand(BookService bookService, Book book) {
        this.bookService = bookService;
        this.book = book;
    }

    @Override
    public void execute() {
        bookService.addBook(book);
    }
}
package com.LibraryMs.LibraryMS.command;
import framework.*;
import com.LibraryMs.LibraryMS.service.BookService;

public class DeleteBookCommand implements Command {
    private BookService bookService;
    private Long id;

    public DeleteBookCommand(BookService bookService, Long id) {
        this.bookService = bookService;
        this.id = id;
    }

    @Override
    public void execute() {
        bookService.deleteBook(id);
    }
}
package com.LibraryMs.LibraryMS.command;

import framework.*;

import com.LibraryMs.LibraryMS.model.Book;
import com.LibraryMs.LibraryMS.service.BookService;

public class UpdateBookCommand implements Command {
    private BookService bookService;
    private Long id;
    private Book updatedBook;

    public UpdateBookCommand(BookService bookService, Long id, Book updatedBook) {
        this.bookService = bookService;
        this.id = id;
        this.updatedBook = updatedBook;
    }

    @Override
    public void execute() {
        bookService.updateBook(id, updatedBook);
    }
}




