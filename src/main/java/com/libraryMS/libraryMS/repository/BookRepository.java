package com.LibraryMs.LibraryMS.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.LibraryMs.LibraryMS.model.Book;

public interface BookRepository extends JpaRepository<Book, Long> {
}
