package com.LibraryMs.LibraryMS.service;
import framework.*;
import com.LibraryMs.LibraryMS.model.Book;
import com.LibraryMs.LibraryMS.repository.BookRepository;
import com.LibraryMs.LibraryMS.strategy.AuthorSearchStrategy;
// import com.LibraryMs.LibraryMS.strategy.SearchStrategy;
import com.LibraryMs.LibraryMS.strategy.TitleSearchStrategy;

// import net.bytebuddy.dynamic.DynamicType.Builder.FieldDefinition.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.SearchStrategy;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {
    @Autowired
    private BookRepository bookRepository;

    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    public void addBook(Book book) {
        bookRepository.save(book);
    }

    public Book getBookById(Long id) {
        return bookRepository.findById(id).orElse(null);
    }

    public Book updateBook(Long id, Book updatedBook) {
        Optional<Book> existingBookOpt = bookRepository.findById(id);
        if (existingBookOpt.isPresent()) {
            Book existingBook = existingBookOpt.get();
            existingBook.setTitle(updatedBook.getTitle());
            existingBook.setAuthor(updatedBook.getAuthor());
            existingBook.setIsbn(updatedBook.getIsbn());
            existingBook.setreservedBy(updatedBook.getreservedBy());
            existingBook.setReserved(updatedBook.isReserved());
            return bookRepository.save(existingBook);
        } else {
            throw new RuntimeException("Book not found with id: " + id);
        }
    }

    public void deleteBook(Long id) {
        bookRepository.deleteById(id);
    }

    public List<Book> searchBooks(String query, String type) {
        TitleSearchStrategy strategy;
        switch (type.toLowerCase()) {
            case "title":
                strategy = new TitleSearchStrategy();
                break;
            case "author":
                strategy = new AuthorSearchStrategy();
                break;
            default:
                throw new IllegalArgumentException("Invalid search type: " + type);
        }
        List<Book> allBooks = bookRepository.findAll();
        return strategy.search(allBooks, query);
    }

    // public void addBook(Book book) {
    // throw new UnsupportedOperationException("Unimplemented method 'addBook'");
    // }
}
