package com.LibraryMs.LibraryMS.controller;

import com.LibraryMs.LibraryMS.command.AddBookCommand;
import com.LibraryMs.LibraryMS.command.DeleteBookCommand;
import com.LibraryMs.LibraryMS.command.UpdateBookCommand;
import com.LibraryMs.LibraryMS.model.Book;
import com.LibraryMs.LibraryMS.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/books")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping
    @ResponseBody
    public List<Book> getAllBooks() {
        return bookService.getAllBooks();
    }

    // @PostMapping
    // @ResponseBody
    // public Book addBook(@RequestBody Book book) {
    // return bookService.addBook(book.getTitle(), book.getAuthor(), book.getIsbn(),
    // book.isReserved());
    // }

    @GetMapping("/{id}")
    @ResponseBody
    public Book getBookById(@PathVariable Long id) {
        return bookService.getBookById(id);
    }

    @GetMapping("/index")
    public String showIndex() {
        return "index"; // This should map to index.html in the templates directory
    }

    @GetMapping("/form")
    public String showForm() {
        return "Books"; // This should map to Books.html in the templates directory
    }

    @GetMapping("/details")
    public String showdetails() {
        return "search"; // This should map to search.html in the templates directory
    }

    @GetMapping("/searchBooks")
    @ResponseBody
    public List<Book> searchBooks(@RequestParam String query, @RequestParam String type) {
        return bookService.searchBooks(query, type);
    }

    @PostMapping
    @ResponseBody
    public Book addBook(@RequestBody Book book) {
        AddBookCommand command = new AddBookCommand(bookService, book);
        command.execute();
        return book;
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> updateBook(@PathVariable Long id, @RequestBody Book updatedBook) {
        UpdateBookCommand command = new UpdateBookCommand(bookService, id, updatedBook);
        command.execute();
        return ResponseEntity.ok("Book updated successfully");
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteBook(@PathVariable Long id) {
        DeleteBookCommand command = new DeleteBookCommand(bookService, id);
        command.execute();
        return ResponseEntity.ok("Book deleted successfully");
    }
}
