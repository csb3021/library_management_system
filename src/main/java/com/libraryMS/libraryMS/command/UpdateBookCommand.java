package com.LibraryMs.LibraryMS.command;

import framework.*;

import com.LibraryMs.LibraryMS.model.Book;
import com.LibraryMs.LibraryMS.service.BookService;

public class UpdateBookCommand implements Command {
    private BookService bookService;
    private Long id;
    private Book updatedBook;

    public UpdateBookCommand(BookService bookService, Long id, Book updatedBook) {
        this.bookService = bookService;
        this.id = id;
        this.updatedBook = updatedBook;
    }

    @Override
    public void execute() {
        bookService.updateBook(id, updatedBook);
    }
}