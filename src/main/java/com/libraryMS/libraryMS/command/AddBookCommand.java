package com.LibraryMs.LibraryMS.command;
import framework.*;
import com.LibraryMs.LibraryMS.model.Book;
import com.LibraryMs.LibraryMS.service.BookService;

public class AddBookCommand implements Command {
    private BookService bookService;
    private Book book;

    public AddBookCommand(BookService bookService, Book book) {
        this.bookService = bookService;
        this.book = book;
    }

    @Override
    public void execute() {
        bookService.addBook(book);
    }
}
