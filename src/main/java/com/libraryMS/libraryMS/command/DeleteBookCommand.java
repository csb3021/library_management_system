package com.LibraryMs.LibraryMS.command;
import framework.*;

import com.LibraryMs.LibraryMS.service.BookService;

public class DeleteBookCommand implements Command {
    private BookService bookService;
    private Long id;

    public DeleteBookCommand(BookService bookService, Long id) {
        this.bookService = bookService;
        this.id = id;
    }

    @Override
    public void execute() {
        bookService.deleteBook(id);
    }
}
